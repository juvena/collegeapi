// Load required packages
var mongoose = require('mongoose');

// Define our beer schema
var CourseSchema = new mongoose.Schema({
  name: String,
  semester: String,
  professor: String,
  difficult: String,
  start_time: String,
  end_time: String,
  grade: String,
  status: String,
  userId: String

});

// Export the Mongoose model
module.exports = mongoose.model('Course', CourseSchema);