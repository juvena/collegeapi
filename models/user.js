// Load required packages
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

// Define our beer schema
var UserSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: true  
  },
  cpf: {
    type: String,
    unique: true,
    required: true
  },
  semester: {
    type: String,
    required: true
    },
  degree_name: {
    type: String,
    required: true
    },
  pass: {
      type: String,
      required: true
    }
});

// Execute before each user.save() call
UserSchema.pre('save', function(callback) {
  var user = this;

  // Break out if the password hasn't changed
  if (!user.isModified('pass')) return callback();

  // Password changed so we need to hash it
  bcrypt.genSalt(10, function(err, salt) {
    if (err) return callback(err);

    bcrypt.hash(user.pass, salt, null, function(err, hash) {
      if (err) return callback(err);
      user.pass = hash;
      callback();
    });
  });
});

UserSchema.methods.verifyPassword = function(pass, cb) {
  bcrypt.compare(pass, this.pass, function(err, isMatch) {
    if (err){ 
        return cb(err);
    }
    cb(null, isMatch);
  });
};
// Export the Mongoose model
module.exports = mongoose.model('User', UserSchema);