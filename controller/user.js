// Load required packages
var User = require('../models/user');

// Create endpoint /api/users for POST
exports.postUsers = function(req, res) {
  var user = new User({
    name: req.body.name,
    pass: req.body.pass,
    semester: req.body.semester,
    cpf: req.body.cpf,
    degree_name: req.body.degree_name,
  });

  user.save(function(err) {
    if (err)
      res.send(err);

    res.json({ message: 'New user to the College' });
  });
};

// Create endpoint /api/user for GET
exports.getUser = function(req, res) {
    User.findOne({cpf: req.user.cpf}, function(err, user) {
    if (err)
      res.send(err);

    response = {name: user.name, cpf: user.cpf, degree_name: user.degree_name, degree_code: user.cpf, semester: user.semester}
    res.json(response);
  });
};