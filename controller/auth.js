// Load required packages
var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;
var User = require('../models/user');

passport.use(new BasicStrategy(
  function(cpf, pass, callback) {
    User.findOne({ cpf: cpf }, function (err, user) {
      if (err) { return callback(err); }

      // No user found with that username
      if (!user) { return callback("username/password incorrect", false); }

      // Make sure the password is correct
      user.verifyPassword(pass, function(err, isMatch) {
        if (err) { 
            return callback(err); }

        // Password did not match
        if (!isMatch) { return callback("username/password incorrect", false); }

        // Success
        return callback(null, user);
      });
    });
  }
));

exports.isAuthenticated = passport.authenticate('basic', { session : false });

exports.getAuthenticated = passport.authenticate('basic', { session : false });
