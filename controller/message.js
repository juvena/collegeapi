// Load required packages
var Message = require('../models/message')

// Create endpoint /api/message for POSTS
exports.postMessage = function(req, res) {
  // Create a new instance of the message model
  var message = new Message();

  // Set the message properties that came from the POST data
  message.message = req.body.value;
  message.userId = req.user._id;

  // Save the message and check for errors
  message.save(function(err) {
    if (err)
      res.send(err);

    res.json({ message: 'message added to the college!', data: message });
  });
 
};

// Create endpoint /api/message for GET
exports.getMessages = function(req, res) {
   var query = Message.find({}).select('-_id -__v -userId');
   // Use the message model to find all message
    query.exec(function (err, message) {
        if (err) res.send(err);
        res.json(message);
    });
};
