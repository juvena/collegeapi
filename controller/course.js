// Load required packages
var Course = require('../models/course')

// Create endpoint /api/courses for POSTS
exports.postCourses = function(req, res) {
  // Create a new instance of the Course model
  var course = new Course();

  // Set the course properties that came from the POST data
  course.name = req.body.name;
  course.semester = req.body.semester;
  course.professor = req.body.professor;
  course.difficult = req.body.difficult;
  course.start_time = req.body.start_time;
  course.end_time = req.body.end_time;
  course.grade = req.body.grade;
  course.status = req.body.status;
  course.userId = req.user._id;

  // Save the course and check for errors
  course.save(function(err) {
    if (err)
      res.send(err);

    res.json({ message: 'Course added to the college!', data: course });
  });
 
};

// Create endpoint /api/courses for GET
exports.getCourses = function(req, res) {
   var query = Course.find({}).select('-_id -__v -userId');
   // Use the Course model to find all course
    query.exec(function (err, courses) {
        if (err) res.send(err);
        res.json(courses);
    });
};

// Create endpoint /api/courses for GET
exports.getCoursesBySemester = function(req, res) {
  // Use the Course model to find all course{"status": {$ne: "Cursando"}}
  var query = Course.find({status: {$ne: "Cursando"}}).select('-_id -__v -userId');

  query.exec(function (err, courses) {
        if (err) res.send(err);
        
      coursesGroup = groupBySemester(courses);
      res.json(coursesGroup);
    });

};

function groupBySemester(courses){
  var coursesBySemester = new Array();
  for(var course of courses){
    index = (course.semester - 1)
    
    if (typeof coursesBySemester[index] == 'undefined'){  
      coursesBySemester[index] = new Object();
      coursesBySemester[index].semester = course.semester
      coursesBySemester[index].courses = new Array();
      coursesBySemester[index].courses.push({
        status: course.status,
        grade: course.rade,
        end_time: course.end_time,
        start_time: course.start_time,
        difficult: course.difficult,
        professor: course.professor,
        semester: course.semester,
        name: course.name
      });
 
    }else{
      coursesBySemester[index].courses.push({
        status: course.status,
        grade: course.rade,
        end_time: course.end_time,
        start_time: course.start_time,
        difficult: course.difficult,
        professor: course.professor,
        semester: course.semester,
        name: course.name
      });
    }
    
  }
  //console.log(coursesBySemester);
  return coursesBySemester;
}

// Create endpoint /api/courses/:course_id for GET
exports.getCourse = function(req, res) {
  // Use the Course model to find a specific course
   var query = Course.find({semester: req.params.semester}).select('-_id -__v -userId');
   // Use the Course model to find all course
    query.exec(function (err, courses) {
        if (err) res.send(err);
        res.json(courses);
    });

};