### What is the point?

Collegeapí is an REST API to be consumed by the college ionic app. this project uses Node.js and Express.js framework with Mongoose.js for working with MongoDB. For access control this project initially use Basic and Passport.js.

### Running the project:
You need to have installed Node.js and MongoDB

### Install dependencies
To install dependencies enter project folder and run following command:
```bash
$ npm install
```

###Run server

To run server execute:
```bash
$ node server.js
```

###Make Requests

##GET Methods:

Getting an User:
```bash
$ http GET http://<user>:<pass>@localhost:3000/api/user
```
Get records of all courses for an user
```bash
$ http GET http://<user>:<pass>@localhost:3000/api/courses
```
Get records of all courses for an user grouped by semester
```bash
$ http GET http://<user>:<pass>@localhost:3000/api/history
```
Get records of all courses in the current semester
```bash
$ http GET http://<user>:<pass>@localhost:3000/api/courses/<semester-number>
```

##POST Methods:
Creating an user:
```bash
$ http POST http://localhost:3000/api/user
```
Creating courses for the user logged in.
```bash
$ http POST http://<user>:<pass>@localhost:3000/api/courses
```
###Recovering Database
There are two .json files in root directory of this project which refers to mongo database tables backups (**courses.json, users,json**).

To recover these tables, type the following command:
```bash
$ mongoimport -d collegeapi -c courses --file courses.json
$ mongoimport -d collegeapi -c users --file users.json
```
The database name and table names are exactly how they are defined in the project.

###References
* [Scotch IO](https://scotch.io)
* [Scott Smith](http://scottksmith.com)