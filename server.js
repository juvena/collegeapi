// Get the packages we need
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var passport = require('passport');
var cors = require('cors');

//Controllers
var courseController = require('./controller/course');
var messageController = require('./controller/message');
var userController = require('./controller/user');
var authController = require('./controller/auth');

//Models
var User = require('./models/user');
var Course = require('./models/course');

// Connect to the collegeapi MongoDB
mongoose.connect('mongodb://localhost:27017/collegeapi');

// Create our Express application
var app = express();

// Use the body-parser package in our application
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(cors());

app.use(function(req, res, next) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header('Access-Control-Allow-Methods', 'GET, POST');
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   next();
});

// Use environment defined port or 3000
var port = process.env.PORT || 3000;

// Create our Express router
var router = express.Router();

// Initial dummy route for testing
// http://localhost:3000/api
router.get('/', function(req, res) {
  res.json({ message: 'You are running college_api' });
});

// Create our Express router
var router = express.Router();

router.route('/history')
  .get(authController.isAuthenticated, courseController.getCoursesBySemester);

// Create a new route with the prefix /courses
router.route('/courses')
  .post(authController.isAuthenticated, courseController.postCourses)
  .get(authController.isAuthenticated, courseController.getCourses);

// Create a new route with the /courses/:course_id prefix
router.route('/courses/:semester')
  .get(authController.isAuthenticated, courseController.getCourse);

router.route('/user')
  .get(authController.isAuthenticated, userController.getUser);

router.route('/message')
  .post(authController.isAuthenticated, messageController.postMessage)
  .get(authController.isAuthenticated, messageController.getMessages);

// Register all our routes with /api
app.use('/api', router);

// Start the server
app.listen(port);
console.log('Insert course on port ' + port);